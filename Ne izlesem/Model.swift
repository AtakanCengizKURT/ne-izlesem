//
//  Model.swift
//  Ne izlesem
//
//  Created by Atakan Cengiz KURT on 29.07.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import Foundation

class VeriCekme{
    
    var filmPuanArray:[String]
    var filmIsimArray:[String]
    var filmTurArray:[String]
    var filmYilArray:[String]
    var filmOzetArray:[String]
    var filmImageArray:[String]
    var filmFiltreArray:[String]
    var filmFragmanArray:[String]


    init() {
        self.filmPuanArray = []
        self.filmIsimArray = []
        self.filmTurArray = []
        self.filmYilArray = []
        self.filmOzetArray = []
        self.filmImageArray = []
        self.filmFiltreArray = []
        self.filmFragmanArray = []
    
    }
}
