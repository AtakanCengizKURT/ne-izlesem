//
//  fragmanViewController.swift
//  Ne izlesem
//
//  Created by Atakan Cengiz KURT on 6.06.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class fragmanViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var fragmanWebView: UIWebView!
    
    let fragman = fragmanFilmIsim
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activity1.stopAnimating()
        let url = URL(string: "\(fragman)")
        let request = URLRequest(url:url!)
        fragmanWebView.loadRequest(request)
        
    }

    @IBOutlet weak var activity1: UIActivityIndicatorView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }

    func webViewDidStartLoad(_ webView: UIWebView)
    {
        activity1.startAnimating()
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activity1.stopAnimating()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
