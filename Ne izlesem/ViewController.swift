//
//  ViewController.swift
//  Ne izlesem
//
//  Created by Atakan Cengiz KURT on 4.06.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import SystemConfiguration
import FirebaseDatabase
import GoogleMobileAds


var filmIsimTable = [String]()
var filmPuanTable = [String]()
var filmTurTable = [String]()
var filmYilTable = [String]()
var filmOzetTable = [String]()
var filmImageTable = [String]()
var filmFragmanTable = [String]()
var filmFiltreTable = [String]()


var fragmanFilmIsim = String()

class ViewController: UIViewController, GADBannerViewDelegate {

    
    
    
    var veriCekme = VeriCekme()
    
    @IBOutlet weak var googleBanner: GADBannerView!
    //yıldızlar
    @IBOutlet weak var r1: UIImageView!
    @IBOutlet weak var r2: UIImageView!
    @IBOutlet weak var r3: UIImageView!
    @IBOutlet weak var r4: UIImageView!
    @IBOutlet weak var r5: UIImageView!
    @IBOutlet weak var r6: UIImageView!
    @IBOutlet weak var r7: UIImageView!
    @IBOutlet weak var r8: UIImageView!
    @IBOutlet weak var r9: UIImageView!
    @IBOutlet weak var r10: UIImageView!
    @IBOutlet weak var w1: UIImageView!
    @IBOutlet weak var w2: UIImageView!
    @IBOutlet weak var w3: UIImageView!
    @IBOutlet weak var w4: UIImageView!
    @IBOutlet weak var w5: UIImageView!
    @IBOutlet weak var w6: UIImageView!
    @IBOutlet weak var w7: UIImageView!
    @IBOutlet weak var w8: UIImageView!
    @IBOutlet weak var w9: UIImageView!
    @IBOutlet weak var w10: UIImageView!
    
    @IBOutlet weak var begenButon: UIButton!
    
    @IBOutlet weak var filmImage: UIImageView!
    @IBOutlet weak var filmPuan: UILabel!
    @IBOutlet weak var filmTur: UILabel!
    @IBOutlet weak var filmYil: UILabel!
    @IBOutlet weak var filmIsim: UILabel!
    @IBOutlet weak var filmOzet: UILabel!
    @IBOutlet weak var yanMenu: UIView!
    @IBOutlet weak var yanMenuLeading: NSLayoutConstraint!
   
   
    
    var filmFragman = String()
    var filmImageURLi = String()
    var menuGoster = false
    
    
    
    
    
    var ref: FIRDatabaseReference!
    
    override func viewDidAppear(_ animated: Bool) {
        

        userDefaultStringArrayBegen()
        
        if filmIsimTable.index(of: filmIsim.text!) != nil {
            begenButon.setImage(UIImage(named: "begenkirmizi") as UIImage?, for: UIControlState.normal)
        }
        
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
//        yanMenu.layer.shadowOpacity = 1
//        yanMenu.layer.shadowRadius = 6
//        yanMenu.layer.shadowOffset = CGSize(width: 5, height: 0)
        
        
        //Google Reklam
        let googleRequest = GADRequest()
        googleRequest.testDevices = [kGADSimulatorID]
        
        googleBanner.adUnitID = "ca-app-pub-1299028299020213/1057701087"
        googleBanner.rootViewController = self
        googleBanner.delegate = self
        googleBanner.load(googleRequest)
        //Google Reklam
        
        
       
        
        var sayi = Int()
        ref = FIRDatabase.database().reference().child("film")
        ref.observe(.value, with: { (snapshot) in
           sayi = Int(snapshot.childrenCount)
            
        })
        
        ref?.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let item = snapshot.value as? NSArray
            {
                for i in 0..<sayi{
                if let filmJson = item[i] as? NSDictionary
                {
                    if let filmIsimVeri = filmJson["isim"] as? String{
                        if (self.veriCekme.filmIsimArray.index(of: filmIsimVeri) == nil)   {
                        self.veriCekme.filmIsimArray.append(filmIsimVeri)
                            
                            if let filmOzetVeri = filmJson["ozet"] as? String{
                                
                                self.veriCekme.filmOzetArray.append(filmOzetVeri)
                                
                                
                            }
                            if let filmPuanVeri = filmJson["puan"] as? Float{
                                self.veriCekme.filmPuanArray.append(filmPuanVeri.description)
                                
                            }
                            if let filmImageVeri = filmJson["image"] as? String{
                                self.veriCekme.filmImageArray.append(filmImageVeri)
                            }
                            if let filmFragmanVeri = filmJson["fragman"] as? String{
                                self.veriCekme.filmFragmanArray.append(filmFragmanVeri)
                            }
                            if let filmTurVeri = filmJson["tur"] as? String{
                                self.veriCekme.filmTurArray.append(filmTurVeri)
                            }
                            if let filmYilVeri = filmJson["yil"] as? Int{
                                self.veriCekme.filmYilArray.append(filmYilVeri.description)
                            }
                            if let filmFiltreVeri = filmJson["yil"] as? String{
                                self.veriCekme.filmFiltreArray.append(filmFiltreVeri)
                            }
                        }
                    }
                   
                    
                    
                }
                }
            }
            
            
          self.filmDegistir()
        })
       
        
       
    }
   


   

    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    @IBAction func menu(_ sender: Any) {
        if menuGoster{
            yanMenuLeading.constant = -310
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }else {
            yanMenuLeading.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        menuGoster = !menuGoster
        
    }
    @IBAction func yanMenuNeizlesem(_ sender: Any) {
        yanMenuLeading.constant = -310
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
         menuGoster = !menuGoster

    }
    
    @IBAction func degistir(_ sender: Any) {
        
        filmDegistir()
       
        
    }
    
    @IBAction func fragman(_ sender: Any) {
        
        //fragman butonu
        if isInternetAvailable() == false
        {
            alertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!")
        }else{
        fragmanFilmIsim = veriCekme.filmFragmanArray[veriCekme.filmIsimArray.index(of: filmIsim.text!)!]
        performSegue(withIdentifier: "fragman", sender: self)
        }
    }
    
    
    @IBAction func paylas(_ sender: Any) {
        
        //paylas butonu
        if isInternetAvailable() == false
        {
            alertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!")
        }else{
            print("paylaş")
        }
    }
    
    
    
    @IBAction func begen(_ sender: Any) {
        
        if isInternetAvailable() == false
        {
            alertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!")
        }else{
        
            if (filmIsimTable.index(of: filmIsim.text!) == nil)   {
        filmPuanTable.append(self.filmPuan.description)
        filmIsimTable.append(self.filmIsim.text!)
        filmTurTable.append(self.filmTur.text!)
        filmYilTable.append(self.filmYil.text!)
        filmOzetTable.append(self.filmOzet.text!)
        filmImageTable.append(self.filmImageURLi)
        filmFragmanTable.append(self.filmFragman)
        
        userDefaultSetBegen()
            
        

            
        }else{
            let indexNo = filmIsimTable.index(of: self.filmIsim.text!)
            filmIsimTable.remove(at: indexNo!)
            filmPuanTable.remove(at: indexNo!)
            filmTurTable.remove(at: indexNo!)
            filmYilTable.remove(at: indexNo!)
            filmOzetTable.remove(at: indexNo!)
            filmImageTable.remove(at: indexNo!)
            
            userDefaultSetBegen()
            
            
            
        }
            
        begenButton()
        
        }
    }
    
    func begenButton(){
        if (filmIsimTable.index(of: filmIsim.text!) != nil){
            let image = UIImage(named: "Begen")?.withRenderingMode(.alwaysTemplate)
            begenButon.setImage(image, for: .normal)
            begenButon.tintColor = UIColor.red
        }else{
            let image = UIImage(named: "Begen")?.withRenderingMode(.alwaysTemplate)
            begenButon.setImage(image, for: .normal)
            begenButon.tintColor = UIColor.white
        }
    }
    
    func filmDegistir(){
        
        
        
        if isInternetAvailable() == false
        {
            alertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!")
        }else{
            
        
        let rastgeleSayi = arc4random_uniform(UInt32(veriCekme.filmIsimArray.count))
        let rasgeleSayi = Int(rastgeleSayi)
        let filmPuan = veriCekme.filmPuanArray[rasgeleSayi]
        self.filmPuan.text = "\(filmPuan) /10"
        let filmIsim = veriCekme.filmIsimArray[rasgeleSayi]
        self.filmIsim.text = filmIsim
        let filmTur = veriCekme.filmTurArray[rasgeleSayi]
        self.filmTur.text = filmTur
        let filmYil = veriCekme.filmYilArray[rasgeleSayi]
        self.filmYil.text = filmYil
        let filmOzet = veriCekme.filmOzetArray[rasgeleSayi]
        self.filmOzet.text = filmOzet
        let filmFragman = veriCekme.filmFragmanArray[rasgeleSayi]
        self.filmFragman = filmFragman
        
        begenButton()

        let filmImageUrl = URL(string: "\(veriCekme.filmImageArray[rasgeleSayi])")
        self.filmImageURLi = veriCekme.filmImageArray[rasgeleSayi]
        let filmImageData = NSData(contentsOf: filmImageUrl!) // nil
        self.filmImage.image = UIImage(data: filmImageData! as Data)
        
        
        let stars:Double = Double(filmPuan)!
        let star:Int = Int(stars)
        
        w10.isHidden = false
        w9.isHidden = false
        w8.isHidden = false
        w7.isHidden = false
        w6.isHidden = false
        w5.isHidden = false
        w4.isHidden = false
        w3.isHidden = false
        w2.isHidden = false
        w1.isHidden = false
        r10.isHidden = false
        r9.isHidden = false
        r8.isHidden = false
        r7.isHidden = false
        r6.isHidden = false
        r5.isHidden = false
        r4.isHidden = false
        r3.isHidden = false
        r2.isHidden = false
        r1.isHidden = false
        
        
        switch star {
        case 10: w10.isHidden = true
            fallthrough
        case 9: w9.isHidden = true
            fallthrough
        case 8: w8.isHidden = true
            fallthrough
        case 7: w7.isHidden = true
            fallthrough
        case 6: w6.isHidden = true
            fallthrough
        case 5: w5.isHidden = true
            fallthrough
        case 4: w4.isHidden = true
            fallthrough
        case 3: w3.isHidden = true
            fallthrough
        case 2: w2.isHidden = true
            fallthrough
        case 1: w1.isHidden = true
            
        default:
            print("imdb puanı yanlış") }
        
        switch star {
        case 1: r2.isHidden = true
            fallthrough
        case 2: r3.isHidden = true
            fallthrough
        case 3: r4.isHidden = true
            fallthrough
        case 4: r5.isHidden = true
            fallthrough
        case 5: r6.isHidden = true
            fallthrough
        case 6: r7.isHidden = true
            fallthrough
        case 7: r8.isHidden = true
            fallthrough
        case 8: r9.isHidden = true
            fallthrough
        case 9: r10.isHidden = true
        
            fallthrough
            
        case 10: if Int(filmPuan) == 10 {
            r10.isHidden = false
        }else {
            r10.isHidden = true
            }
        default:
            print("imdb puanı yanlış")
        }
        }
    }
    
    
    //internet kontrolü
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    //internet kontrolü
    
    
    //Alert
    func alertController(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        let btnCancel = UIAlertAction(title: "Tamam", style: UIAlertActionStyle.cancel) { (ACTION) in
            
        }
        alert.addAction(btnCancel)
    }
    //Alert
    
    func userDefaultSetBegen(){
        UserDefaults.standard.set(filmPuanTable, forKey: "filmPuanBegen")
        UserDefaults.standard.set(filmIsimTable, forKey: "filmIsimBegen")
        UserDefaults.standard.set(filmTurTable, forKey: "filmTurBegen")
        UserDefaults.standard.set(filmYilTable, forKey: "filmYilBegen")
        UserDefaults.standard.set(filmOzetTable, forKey: "filmOzetBegen")
        UserDefaults.standard.set(filmImageTable, forKey: "filmImageBegen")
        UserDefaults.standard.set(filmFragmanTable, forKey: "filmFragmanBegen")
    }
    
    func userDefaultStringArrayBegen(){
        filmPuanTable = UserDefaults.standard.stringArray(forKey: "filmPuanBegen")!
        filmIsimTable = UserDefaults.standard.stringArray(forKey: "filmIsimBegen")!
        filmTurTable = UserDefaults.standard.stringArray(forKey: "filmTurBegen")!
        filmYilTable = UserDefaults.standard.stringArray(forKey: "filmYilBegen")!
        filmOzetTable = UserDefaults.standard.stringArray(forKey: "filmOzetBegen")!
        filmImageTable = UserDefaults.standard.stringArray(forKey: "filmImageBegen")!
        filmFragmanTable = UserDefaults.standard.stringArray(forKey: "filmFragmanBegen")!

    }
}

