//
//  BegendiklerimViewController.swift
//  Ne izlesem
//
//  Created by Atakan Cengiz KURT on 4.06.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class BegendiklerimViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var begendiklerimTableView: UITableView!
    @IBOutlet weak var yanMenuLeading: NSLayoutConstraint!
    @IBOutlet weak var yanMenu: UIView!
    @IBOutlet weak var begendiklerim: UIButton!
    
    var menuGoster = false
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    @IBAction func begendiklerim(_ sender: Any) {
        yanMenuLeading.constant = -310
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
        menuGoster = !menuGoster
        
    }
    
    @IBAction func menu(_ sender: Any) {
        if menuGoster{
            yanMenuLeading.constant = -310
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }else {
            yanMenuLeading.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        menuGoster = !menuGoster
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    
    return filmIsimTable.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text = filmIsimTable[indexPath.row]
        
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            filmIsimTable.remove(at: indexPath.row)
            filmPuanTable.remove(at: indexPath.row)
            filmOzetTable.remove(at: indexPath.row)
            filmTurTable.remove(at: indexPath.row)
            filmYilTable.remove(at: indexPath.row)
            filmImageTable.remove(at: indexPath.row)
            filmFragmanTable.remove(at: indexPath.row)
            
            begendiklerimTableView.reloadData()
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        begendiklerimTableView.reloadData()
    }
}
